const express = require('express')
const app = express()
const port = 3000
const fs = require("fs");
const route = require('./router/index')

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

route(app)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})