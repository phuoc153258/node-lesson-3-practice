const { body, param, query ,validationResult } = require('express-validator');

const getBlogCondition = () => {
    return [
        param('id').isString().notEmpty().isLength({min: 5})
    ]
}

const createBlogCondition = () => {
    return [
        body('title').isString().notEmpty().isLength({max:50}),
        body('subTitle').isString().notEmpty().isLength({max:100}),
        body('content').isString().notEmpty().isLength({max:1000}),
        body('authorId').isString().notEmpty().isLength({max:50}),
        body('isPublic').notEmpty().isBoolean(),
        body('createdAt').toDate().isISO8601().notEmpty(),
        body('updatedAt').toDate().isISO8601().notEmpty()
    ]
}

const updateBlogCondition = () => {
    return [
        query('id').isString().notEmpty().isLength({max:50}),
        query('authorId').isString().notEmpty().isLength({max:50}),
        body('title').isString().notEmpty().isLength({max:50}),
        body('subTitle').isString().notEmpty().isLength({max:100}),
        body('content').isString().notEmpty().isLength({max:1000}),
        body('isPublic').notEmpty().isBoolean(),
        body('createdAt').toDate().isISO8601().notEmpty(),
        body('updatedAt').toDate().isISO8601().notEmpty(),
    ]
}

const deleteBlogCondition = () => {
    return [
        query('id').isString().notEmpty().isLength({max:50}),
        query('authorId').isString().notEmpty().isLength({max:50})
    ]
}

const getBlogMiddleware = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next()
}

const createBlogMiddleware = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next()
}

const updateBlogMiddleware = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next()
}

const deleteBlogMiddleware = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next()
}

module.exports = {
    getBlogCondition, getBlogMiddleware,
    updateBlogCondition, createBlogMiddleware, 
    createBlogCondition, updateBlogMiddleware,
    deleteBlogCondition, deleteBlogMiddleware
}