const userRouter = require('./users')
const blogRouter = require('./blogs')

function route(app) {

    app.use('/users', userRouter)

    app.use('/blogs', blogRouter)

    app.use('/', (req, res) => {
        res.send('Hello World!')
    });

}
module.exports = route;