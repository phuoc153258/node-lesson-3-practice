const express = require('express');
const router = express.Router();
const validation = require('../middleware/blogValidationMiddleware')
const blogController = require('../controller/blog.controller');

router.post('/', validation.createBlogCondition(), validation.createBlogMiddleware , blogController.createBlog)
router.put('/', validation.updateBlogCondition(), validation.updateBlogMiddleware , blogController.updateBlog)
router.delete('/', validation.deleteBlogCondition(), validation.deleteBlogMiddleware , blogController.deleteBlog)
router.get('/:id', validation.getBlogCondition(), validation.getBlogMiddleware , blogController.getBlog)

module.exports = router;