const { readUser,createUser, updateUser, deleteUser} = require('../service/user')

module.exports = {
    createUser: function(req,res){
        res.send(createUser(req))
    },
    getUser: function (req,res){
        res.send(readUser())
    },
    updateUser: function (req, res){
        res.send(updateUser(req))
    },
    deleteUser: function (req, res){
        res.send(deleteUser(req))
    }
};