const { getBlogByAuthorId, createBlog, updateBlog ,deleteBlog} = require('../service/blog')

module.exports = {
    getBlog: function (req,res){
        let result = getBlogByAuthorId(req)
        if( result.length == 0) return res.send("Error AuthorId !!!")
        res.send(result)
    },
    createBlog: function (req,res){
        let result = createBlog(req)
        res.send(result)
    },
    updateBlog: function (req,res){
        let result = updateBlog(req)
        if(result == null) return res.send("Error ID & AuthorID!!!")
        res.send(result)
    },
    deleteBlog: function (req,res){
        let result = deleteBlog(req)
        if(result == null) return res.send("Error ID & AuthorID!!!")
        res.send(result)
    }
};