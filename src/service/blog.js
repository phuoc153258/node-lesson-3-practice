const {readFile, writeFile} = require('../helper/helper')
const { v4: uuidv4 } = require('uuid');
const { body } = require('express-validator');

let getBlogByAuthorId = (req) => {
    let data = readFile()
    let blogs = data.Blogs.filter(blog => blog.authorId == req.params.id)
    return blogs
}

let createBlog = (req) => {
    let data = readFile()
    const blog = {
        id: uuidv4(),
        title: req.body.title,
        subTitle: req.body.subTitle,
        content: req.body.content,
        authorId: req.body.authorId,
        isPublic: req.body.isPublic,
        createdAt: req.body.createdAt,
        updatedAt: req.body.updatedAt
    }
    let isContain = data.Blogs.find(b => b.id == blog.id)
    if(isContain == null) data.Blogs.push(blog) 
    writeFile(JSON.stringify(data))
    return blog
}
let updateBlog = (req) => {
    let data = readFile()
    let blog = data.Blogs.find(blog => blog.id == req.query.id && blog.authorId == req.query.authorId)
    if(blog == null) return null
    blog.title = req.body.title
    blog.subTitle = req.body.subTitle
    blog.content = req.body.content
    blog.isPublic = req.body.isPublic
    blog.createdAt = req.body.createdAt
    blog.updatedAt = req.body.updatedAt
    writeFile(JSON.stringify(data))
    return blog
}
let deleteBlog = (req) => {
    let data = readFile()
    let blog = data.Blogs.find(blog => blog.id == req.query.id && blog.authorId == req.query.authorId)
    if(blog == null) return null
    let newData = {
        Users: data.Users,
        Blogs: data.Blogs.filter( blog => blog.id != req.query.id || blog.authorId != req.query.authorId )
    }
    writeFile(JSON.stringify(newData))
    return blog
}

module.exports = { getBlogByAuthorId, createBlog, updateBlog, deleteBlog}