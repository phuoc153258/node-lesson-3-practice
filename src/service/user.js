const {readFile, writeFile} = require('../helper/helper')
const { v4: uuidv4 } = require('uuid');

let readUser = () => {
    let data = readFile()
    return data.Users
}

let createUser = (req) => {
    let data = readFile()
    const user = {
        name: req.body.name,
        age: req.body.age,
        email: req.body.email,
        id: uuidv4()
    }
    data.Users.push(user)
    writeFile(JSON.stringify(data))
    return user
}

let updateUser = (req) => {
    let data = readFile()
    let userUpdate = data.Users.find(user => user.id == req.params.id)
    userUpdate.name = req.body.name
    userUpdate.age = req.body.age
    userUpdate.email = req.body.email
    writeFile(JSON.stringify(data))
    return userUpdate
}

let deleteUser = (req) => {
    let data = readFile()
    let newData = {
        Users: data.Users.filter(user => user.id != req.params.id),
        Blogs: data.Blogs
    }
    writeFile(JSON.stringify(newData))
    return newData
}

module.exports = { readUser ,createUser , updateUser , deleteUser}