const {readFileSync,writeFileSync} = require("fs");

let readFile = () => {
    let user = JSON.parse(readFileSync('./src/data/db.json'))
    return user
}
let writeFile = (data) => {
    writeFileSync('./src/data/db.json', data);
}

module.exports = { readFile , writeFile }